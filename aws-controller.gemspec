# -*- encoding: utf-8 -*-
require File.expand_path('../lib/aws-controller/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Rivka Tzur"]
  gem.email         = ["rivka.tzur@quicklizard.com"]
  gem.description   = "aws controller"
  gem.summary       = "aws controller"
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "aws-controller"
  gem.require_paths = ["lib"]
  gem.version       = Aws::Controller::VERSION

  gem.add_dependency 'aws-sdk'
end
