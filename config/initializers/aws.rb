require 'yaml'

unless defined?(Aws::Controller)
  module Aws
    module Controller; end
  end
end

aws_config = YAML.load_file(File.join(ENV['HOME'], '.aws.yml'))

aws_config.each do |key, value|
  Aws::Controller.const_set(key.capitalize.to_sym, value)
end