require "aws-sdk"
require_relative "../config/initializers/aws"
require_relative "aws-controller/version"

module Aws
  module Controller
    class AwsController
      attr_accessor :ec2, :vpc, :key_pair, :security_groups, :subnet_id

      def initialize(vpc_id, security_groups, subnet_id, key_pair_name)
        @ec2 = AWS::EC2.new(
          :access_key_id => Aws::Controller::Access_key_id,
          :secret_access_key => Aws::Controller::Secret_access_key)
        @vpc = @ec2.vpcs[vpc_id]
        @security_groups = security_groups
        @subnet_id = subnet_id
        @key_pair = @ec2.key_pairs[key_pair_name]
      end

      #create image from instance
      #Input: instance_id, name
      def create_image(instance_id, name)
        @ec2.images.create(:instance_id => instance_id, :name => name)
        p 'creating image %s from instance %s' % [name, instance_id]
      end

      #create instance from image
      #Input: image_id, app, stage
      def create_instance(image_id, app, stage)
        instance = @ec2.instances.create(:image_id => image_id,
                             :subnet => @subnet_id,
                             :security_groups => @security_groups,
                             :key_pair => @key_pair)
        instance.tags.app = app
        instance.tags.stage = stage
        eip = @ec2.elastic_ips.create(:vpc => @vpc)
        instance.associate_elastic_ip(eip)
      end

      #stop all running instances of given app & stage
      #Input: app, stage
      def stop(app, stage)
        get_instances(app, stage).each do |instance|
          if status == 'running'
            eip = instance.elastic_ip
            eip.disassociate
            eip.release
            instance.stop
          end
        end
      end

      #start all stopped instances of given app & stage
      #Input: app, stage
      def start(app, stage)
        get_instances(app, stage).each do |instance, status|
          if status == 'stopped'
            eip = @ec2.elastic_ips.create(:vpc => @vpc)
            instance.associate_elastic_ip(eip)
            instance.start
          end
        end
      end

      #reboot all instances of given app & stage
      #Input: app, stage
      def reboot(app, stage)
        get_instances(app, stage).each do |instance|
          eip = instance.elastic_ip
          eip.disassociate
          instance.reboot
          instance.associate_elastic_ip(eip)
        end
      end

      #terminate all instances of given app & stage
      #Input: app, stage
      def terminate(app, stage)
        get_instances(app, stage).each do |instance|
          eip = instance.elastic_ip
          eip.disassociate
          eip.release
          instance.terminate
        end
      end

      #get all instances of given app & stage
      #Input: app, stage
      #Output: Hash of all app & stage instances with status({"i-cb46efb7"=>"stopped", "i-1bt6e5bd"=>"running"})
      def get_instances(app , stage)
        p 'getting instances for app: %s, stage: %s' % [app, stage]
        instances = {}
        @ec2.instances.each do |instance|
          if instance.tags.stage == stage and instance.tags.app == app
            instances[instance.id] = instance.status.to_s
          end
        end
        instances
      end
    end
  end
end
