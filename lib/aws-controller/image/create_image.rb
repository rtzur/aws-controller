require_relative '../../aws-controller'
aws = Aws::Controller::AwsController.new(Aws::Controller::Vpc_id, Aws::Controller::Security_groups, Aws::Controller::Subnet_id, Aws::Controller::Key_pair_name)
instance_id = ARGV[0]
name = ARGV[1]
aws.create_image(instance_id, name)