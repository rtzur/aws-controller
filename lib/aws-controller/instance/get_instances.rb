require_relative '../../aws-controller'
aws = Aws::Controller::AwsController.new(Aws::Controller::Vpc_id, Aws::Controller::Security_groups, Aws::Controller::Subnet_id, Aws::Controller::Key_pair_name)
app = ARGV[0]
stage = ARGV[1]
instances = aws.get_instances(app, stage)
p instances