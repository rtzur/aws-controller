# Aws::Controller

A controller for vpc instances

## Installation

Add this line to your application's Gemfile:

    gem 'aws-controller'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install aws-controller

Add this configuration file:

    ~/.aws.yml

Like this:

        vpc_id: ''
        security_groups:
          - ''
        subnet_id: ''
        key_pair_name: ''
        access_key_id: ''
        secret_access_key: ''

## Usage

    *Create image from instance
    *Create new instance from image connected to vpc(+ add tags)
    *Destroy instance
    *List instances of given environment with statuses(app and stage)
    *Restart all instances of given app and environment
    *Stop/start all instances of given app and environment

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
